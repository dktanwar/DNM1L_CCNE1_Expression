!#/bin/bash
find /scratch/malay -name "*.rsem.genes.normalized_results.bz2" -exec /bin/bash extract_gene_expression.sh {} "(DNM1L)|(CCNE1)" \; > ../results/01_norm_scores_dnm1l_ccne1/dnm1L_ccne1.txt
